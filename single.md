# Lookups may be synthesized

The AFDKO Feature File syntax allows some rules that
are not directly supported by the compiled binary OTF format.
In such cases the feature compiler will synthesize suitable
rules.

The AFDKO Feature File Specification gives 3 different formats
for a Single Sub, where a single glyph is replaced by a single
glyph.

These are:

    sub SingleGlyph by OtherSingleGlyph; # Format A
    sub GlyphGroup by SingleGlyph;       # Format B
    sub GlyphGroup by AnotherGlyphGroup; # Format C

Format C is the general form.
The groups should be the same size;
every glyph in the left-hand group is mapped to the glyph in the
corresponding position in the right-hand group.

Format A maps a single glyph to a single glyph, but
is implemented as a group with one item mapping to
another group with one item.

Format B has every glyph in a group mapping to the same single glyph.
But this lookup rule is not directly supported by the binary OTF
format.
The feature compiler implements the rule by creating a
right-hand group that has a single glyph repeated.

## `fext`

`fext` can reveal The Truth:

    # GSUB,version, 1 , 0
    languagesystem DFLT dflt;

    feature liga { # Lookup [0 1 2]
      lookup L1 { #T1(F0)[1]
        sub [A] by [B];  # SS1
      } L1;
      lookup L2 { #T1(F0)[1]
        sub [I J K] by [M M M];  # SS2
      } L2;
      lookup L3 { #T1(F0)[1]
        sub [S T U] by [X Y Z];  # SS1
      } L3;
    } liga ;

Lookup `L1` was in Format A, a single glyph to a single glyph,
but has been compiled as a group to group rule with a group of
size 1.

Lookup `L2` was in Format B, a group to a single glyph;
here we can see the group `[M M M]`,
which did not appear in the source, has been synthesized.

Nerd embellishment: There are 2 binary formats for Single Substitution.
This version of `fext` puts `SS1` in the comment
for Single Substitution Format 1, and `SS2` in the comment
for Single Substitution Format 2.

As a bonus i tried a lookup with the syntax for removing a glyph
from the input. According to the AFDKO Feature File syntax this
is `sub glyphname by NULL`.
Like Format B there is no specific binary format to support this rule,
the feature compiler implements this by using a
Multiple Substitution lookup with an empty replacement sequence.
Curiously, the OpenType specification says that you should not
do this.

# END
