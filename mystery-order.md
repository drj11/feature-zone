# Mystery Order

Investigating a problematic behavious in Ripple System that
defies my current understanding.

In Ripple System there are two sorts of rules:

- _follow rules_ which select
  a glyph according to its preceding context.
- _initial rules_ which select a glyph with no preceding context.

The trial implementation placed these rules in two different lookups,
with the _follow rules_ in the earlier lookup;
reasoning that the earlier lookup would give them precedence.
This is important because we would like the initial rule to only
be applied at the beginning of a line;
in general we would like the follow rules to be applied when they can be.

This however did not work. The entire line was the result of applying init 
rules.

Therefore we create here a simpler system to illustrate.

# END
