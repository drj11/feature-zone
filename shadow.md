# Lookups shadow

Because lookups act in sequence,
an earlier lookup that acts on some particular input
will prevent a later lookup acts on the same input.

Example:

    languagesystem DFLT dflt;

    feature liga {
      lookup toX {
        sub A by X;
      } toX;
      lookup toM {
        sub A by M;
      } toM;
    } liga;

An **A** in the input will be transformed to **X**
by the first lookup (`toX`);
the second lookup, `toM`, will never apply.
It is shadowed (overshadowed really) by
the earlier lookup.

The feature compiler won't warn you of cases like this,
nor does it remove the lookup.

A lookup can be referenced by name, and
in this way used in more than one feature.
In principle the lookup `toM` could be used by another feature
in a way that is not overshadowed.

# END
