# Lookups cascade

The lookups in a feature are in a particular sequence,
and within that sequence,
the output of one lookup is fed into the next.

Consider the feature file,
for now concentrate on the AB and BC rules:

    languagesystem DFLT dflt;

    feature liga {
      lookup AB {
        sub A by B;
      } AB;
      lookup BC {
        sub B by C;
      } BC;

      sub I by J;
      sub J by K;
    } liga ;

This feature has a lookup, `AB`, that turns **A** into **B**,
and a _separate_ and _subsequent_ lookup, `BC`,
that turns **B** into **C**.

Therefore an **A** in the input will appear as a **C**:
it's get converted to **B** by the `AB` lookup rule;
and then gets converted to **C** by the `BC` lookup rule.

Considering the remaining rules in the feature.
What about **I**?

Well, here's the output of `hb-view cascade.otf ABCDIJKL`:

![the text CCCFJKKL](image/cascade.svg)

The **A** appears as a **C**, and so does the **B**.

The **I** got translated to a **J**, and
the **J** next to it got translated to **K**.
But the **I** did not changed all the way to **K**.

To understand why not, we need to learn a couple of things about
the lookup rules.

Rules that appear in a feature that are not inside a lookup are
placed into a lookup by the feature compiler.
Each section of unbracketed rules outside a lookup goes into a
single lookup.
If there are multiple sections between different lookups, each
section will end up in its own lookup.

The other fun-fact is that a lookup will only act once.
There can be multiple rules in a lookup, and
they are run in order.
But once a rule has matched and acted to change the input,
the lookup is exhausted, and
processing continues _with the next_ lookup.


## fext tool

I can use my `fext` tool to inspect the compiled font-file and
illustrate what is going on.
`fext` converts the features that are compiled into a binary OTF
file into source code.
It can't know the exact source code, and is incomplete, but is
still useful.

In the output below, the `#` token indicates a comment.
Everything from the `#` to the end of the line is a comment and
ignored.
Here they are used by `fext` for debugging information and
can be ignored.

This is the output from `fext cascade.otf`:

    # GSUB,version, 1 , 0
    languagesystem DFLT dflt;

    feature liga { # Lookup [0 1 2]
      lookup L1 { #T1(F0)[1]
        sub [A] by [B];  # SS1
      } L1;
      lookup L2 { #T1(F0)[1]
        sub [B] by [C];  # SS1
      } L2;
      lookup L3 { #T1(F0)[1]
        sub [I J] by [J K];  # SS1
      } L3;
    } liga ;

Lookups `L1` and `L2` correspond to lookups `AB` and `BC` in the source.
The names of lookups are compiled-away and not stored in the
binary OTF file, so `fext` creates its own names for them.
Note that `sub A by B` has become `sub [A] by [B]`,
it is more general to use a group even when in this case
the syntax provides a slightly more convenient syntax for
a single glyph
(a later `fext` may remove the square-bois in this case).

Lookup `L3` corresponds to the rules in the source file that are
not syntactically in a lookup.
Once they are compiled into a binary OTF file,
all rules appear in some lookup.
Note both rules have been placed in the same lookup, _and_
the compiler has cunningly spotted that it can use a single
group-based rule instead of two separate ones.
In this form it makes it even more clear that **I** will
not get translated to **K**.

# END
