# Feature Zone

As of 2023 all the examples in the Feature Zone are advanced, or
at least intermediate.
Most of them explain some 2nd-level subtlety of a part of the
OpenType Layout system, and builds upon a more basic
understanding that is not documented here.

[cascade](cascade.md) is an example of how glyphs do, or do not,
cascade through successive lookups.

[shadow](shadow.md) a simple case of shadowing: where an earlier
lookup prevents a later one from running.

[heterogen](heterogen.md) is an exploration of the statement
"All of the lookup rules in a lookup block must be of the same
type"; showing that, they do not.

[multiple](multiple.md) is an exploration of Multiple Substitution
lookups and how they can alias Single Substitution lookup,
causing problems for the mapping between source and binary (and back).


## Airtest

The font file here, _Airtest_, is created specifically
for the purpose of testing and demonstrating OpenType features.

It has a restricted glyph set: A to Z, AE, Thorn, A- O-
U-dieresis, and the numbers 0 to 9.
The glyphs are copied from Airvent.

There are `smol` version of the numbers:
these are copied from Airvent and modified so that:

- they have cut endings instead of rounded
- they have no optical overshoots (range strictly between 0 and 500)
- the first point of the first contour is the leftmost point on
  the baseline (which means that the first pen motion in the
  Charstring is a `hmoveto`)


## Some questions

- can we join em-dashes? Consider an em-dash that has some sort
  of decorative terminal on left and right.
  Can we have a feature so that multiple em-dashes create a
  longer dash that stil only had one set of terminals.
  essentially LmRLmRLmR -> LcccR.


## liga

When we're writing notes on `liga`, may want to look at
[League
Script](https://www.theleagueofmoveabletype.com/league-script).
It has _rst_ and _rs_ ligatures.


## Alternate syntax

An aside on alternate syntaxes.

How about

  sub p r e / i n p / p o s t by i_n_p;

?

# END
