# Multiple Substitution

## Introduction

A _Multiple Substitution_ is a rule where a single glyph
is replaced by a sequence:

    languagesystem DFLT dflt;

    feature liga {
      lookup AE {
        sub AE by A E;
        sub B by X X;
      } AE;
    } liga;

In this example the single glyph `AE` is
replaced by the sequence `A` `E`;
and the single glyph `B` is replaced by the sequence `X` `X`.

The binary representation of a Multiple Substitution allows for
sequences of many and different lengths.
Including length 1.

## Aliases Single Substitution

A Multiple Substitution that replaces a glyph with
a sequence of length 1 has the same effect as a Single
Substitution.

In principle the Feature File compiler could support the
following:

    sub AE by A E;
    sub B by Y;

It would "cleverly" notice that although the second substitution
looked like a Single Substitution, which would violate the
constraint that substitutions within a lookup should all be the
same type, it could replace with Multiple Substitution that has
the same effect.

It's not clever and it doesn't do that, so the above fails to
compile.

With some finagling it is possible to get a Multiple
Substitution with a single glyph replacement in the binary
representation.
It's most easily done by using `ttx`:

- get the OTF file in XML format
- edit the `MultipleSubst`
- use `ttx` again to convert back to OTF

That works, and the OTF file you get is fine.
And `fext` can extract and show the Feature File syntax, but
we get:

    lookup L1 { #T2(F0)[1]
      sub B by X;
      sub AE by A E;
    } L1 ;

The first substitution, `sub B by X`,
has the exact same syntax as a Single Substitution rule.
There is no way around this for `fext`:
Some lookups in the binary representation have no equivalent
AFDKO Feature File syntax.


## Indirect using Contextual Substitution

Bonus feature: Adding an apostrophe fixes everything, because
although it looks like a slightly different syntax for
the same thing,
it actually turns everything into a Contextual Substitution
rule, and as discussed in `heterogen.md` that means the
indirect lookups can be different types.

So the following breaks a constraint:

    sub AE by A E;
    sub B by Y;

This is a Multiple Substitution followed by a Single
Substitution, which breaks the constraint
that rules within a lookup should be the type.
But adding apostrophes makes the rules Contextual Substitutions,
which is proper.

    sub AE' by A E;
    sub B' by Y;


## `by NULL` is a Multiple Substitution

Further Bonus feature: A zero-length Multiple Substitution
is how `by NULL` is implemented.

# END
