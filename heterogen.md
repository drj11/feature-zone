# Heterogeneous lookups

All of the lookup rules in a lookup block must be of the same type.

Consider the following feature file:

    languagesystem DFLT dflt;

    feature liga {
      lookup AE {
        sub AE by A E;
        sub A E by AE;
      } AE;
    } liga;

the first rule is a Multiple Substitution rule;
the second rule is a Ligature Substitution rule.

Since these are different types,
the feature compiler complains and fails the compilation:

    Lookup type different from previous rules in this lookup block

In at least some cases, it is possible to achieve the same
effect by using a (Chaining) Contextual Substitution lookup.
This is because a Contextual Substitution rule refers to another
lookup that performs the substitution on the input.
_Those_ lookups need not be the same type.

It looks like this:


    languagesystem DFLT dflt;

    lookup AE {
      sub AE by A E;
    } AE;
    lookup A_E {
      sub A E by AE;
    } A_E;

    feature liga {
      sub AE' lookup AE;
      sub A' lookup A_E E';
    } liga;

So now the two substitution rules in the `liga` feature are both
Contextual Substitution rules, meaning that they can be in the
same lookup.
Each one refers to a different lookup rule (`AE` and `A_E`),
and _those_ rules are different types.
`AE` is a Multiple Substitution lookup;
`A_E` is a Ligature Substitution lookup.

`fext` has recently been upgraded to cope with these rules,
and can successfully reverse engineer a Feature File from
the OTF file:

    # from context-heterogen.otf
    # converted by fext
    # on 2023-02-16T12:14:26Z
    # GSUB,version, 1 , 0
    languagesystem DFLT dflt ;

    # Standalone lookups for liga
    lookup L2 { #T2(F0)[1]
      sub AE by A E;
    } L2 ;
    feature liga {
      lookup L1 { #T6(F0)[2]
        sub [AE]' lookup L2;
        sub A' E' by AE;
      } L1 ;
      script DFLT ;
    } liga ;

Notice that the chained lookup that is required by the `liga` feature, 
has been formatted before the feature.
Placing it before is required for compilation with `makeotf`.
In principle, this means that the generated feature file
can be compiled with `makeotf`; in practice, sometimes
the output can be compiled with `makeotf`, sometimes there
are other problems.

In this `liga` feature it look likes the second substitution has
been changed. But actually it has not.
The AFDKO Feature File syntax has a special syntax for
Contextual Substitution lookups in the case where only
a single lookup is applied to the input and it is a
Ligature Substitution.

The indirect lookup is still there, but it is implied.

# END
