# The Infinite Ligature

I originally was thinking about this problem for making
infinitely long connected em-dashes that had decorative
terminals.
If the em-dash is a horizontal rectangle then you don't need any
ligatures, just typeset them together and they will make a very
long rectangle.
If the em-dash has some sort of terminal then when you have a
whole bunch together you might want to eliminate the terminals
in the middle, so you can a long continuous bar, with terminals
only at the extreme left and extreme right.

Here i'm going to illustrate how to do that, but using the
letter T instead.

Note that the crossbar of the letter T in Airtest has rounded
ends, and that the T has some amount of set space either side.

Let's say you wanted TT to use a ligature that fuses together
the two crossbars.
That is easily done by creating a `T_T` ligature and creating a
rule:

    sub T T by T_T;

In fact the most popular font design software will automatically
create the rule if you have a glyph named `T_T`.

What if we wanted this to work for TTTT, or _any_ length of T?

Essentially we want to identify left- mid- and right-T (and
create glyphs for those).

    @Tany = [T T.left T.mid T.right];

    lookup Tinfinite {
      sub @Tany T' @Tany by T.mid;
      sub T' @Tany by T.left;
      sub @Tany T' by T.right;
    }
