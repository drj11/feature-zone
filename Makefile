all: OTF/cascade.otf OTF/context.otf OTF/mystery-order.otf OTF/shadow.otf OTF/single.otf OTF/multiple.otf OTF/context-heterogen.otf OTF/infinite-liga.otf OTF/stripe-av.otf

OTF/cascade.otf: feature/feature-cascade.fea Airtest*.otf
	makeotf -ff feature/feature-cascade.fea -f Airtest*.otf -o $@

OTF/context.otf: feature/context.fea Airtest*.otf
	makeotf -ff feature/context.fea -f Airtest*.otf -o $@

OTF/shadow.otf: feature/feature-shadow.fea Airtest*.otf
	makeotf -ff feature/feature-shadow.fea -f Airtest*.otf -o $@

OTF/single.otf: feature/feature-single.fea Airtest*.otf
	makeotf -ff feature/feature-single.fea -f Airtest*.otf -o $@

OTF/multiple.otf: feature/feature-multiple.fea Airtest*.otf
	makeotf -ff feature/feature-multiple.fea -f Airtest*.otf -o $@

OTF/context-heterogen.otf: feature/feature-context-heterogen.fea Airtest*.otf
	makeotf -ff feature/feature-context-heterogen.fea -f Airtest*.otf -o $@

OTF/infinite-liga.otf: feature/feature-infinite-liga.fea Airtest*.otf
	makeotf -ff feature/feature-infinite-liga.fea -f Airtest*.otf -o $@

OTF/stripe-av.otf: feature/feature-stripe-av.fea Airtest*.otf
	makeotf -ff feature/feature-stripe-av.fea -f Airtest*.otf -o $@

OTF/mystery-order.otf: feature/mystery-order.fea Airtest*.otf
	makeotf -ff feature/mystery-order.fea -f Airtest*.otf -o $@
