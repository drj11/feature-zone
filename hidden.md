# Hidden

Is it possible to encode a hidden state in the glyph sequence?

What would be the point?

Imagine a _LiebeHeide_-like font where there are many `e` glyphs
and an input sequence of `e`s results in different `e` glyphs.
Essentially we are trying to achieve a certain "randomness"
(lookup rules can never be random however).
One way to achieve this is with lots of rules.


Another way, if the lookup engine supported it, would be to have
a small amount of hidden state that a lookup rule could refer to
as part of its context.

  state.13 e -> e.1 state.7
  state.7 e -> e.2 state.9

In the above the state.N glyphs represent a hidden state that is
changed with each input glyph. `e.1` and `e.2` represent
variations on the glyph `e`.
The state glyphs are hidden by being empty.

In this hidden-state scheme we use the state to minimise context.
A lookup rule doesn't have to have a large amount of look-behind
context, it can refer to the hidden state.

Imagine the hidden state could be one of 15 values (1 to 15).
Any time we wanted a glyph to vary we could have a lookup table
(up to 15 entries) that specified the output glyph and the new
state.

Ok, but OpenType lookup engines don't support this.
Or do they?

A Contextual Substitution might help.
A Contextual Substitution have a main, outer, lookup and
optionally for each glyph in the matched target sequence,
a nested lookup.
The main lookup and the nested lookup can have
different _flags_.

I haven't mentioned lookup flags yet,
but they let you ignore various classes
(i think they're intended to be used for ignoring marks when
making ligatures of base glyphs, kerning that ignores marks, and
so on).

It may be simpler than that.
Can we put all the state glyphs in a class and then ignore that
class for position and liga and so on?

# END
